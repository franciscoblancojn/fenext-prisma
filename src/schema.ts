export interface Product {
  id: string;
  name: string;
  orders?: ProductOrder[];
}

export interface ProductOrder {
  id: string;
  name: string;
  productId?: string;
  product?: Product;
  quantity: number;
  orderId?: string;
  order?: Order;
}

export interface Order {
  id: string;
  userId: string;
  user: User;
  products?: ProductOrder[];
}

export interface User {
  id: string;
  name: string;
  image?: string;
  role: UserRole;
  order?: Order[];
  card?: Card[];
}

export interface Card {
  id: string;
  name: string;
  number: number;
  userId: string;
  user: User;
}

export enum UserRole {
  USER = 'USER',
  ADMIN = 'ADMIN',
}
